import { Component, OnInit } from '@angular/core';

// ActivatedRoute service needed for accessing route inside onInit 
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})

export class DishdetailComponent implements OnInit {

  dish: Dish;

  constructor( 
    private dishservice: DishService, 
    private route: ActivatedRoute, 
    private location: Location) { }

  ngOnInit() {
    // '+this...' converts id parameter to int
    let id= +this.route.snapshot.params['id'];
    this.dish = this.dishservice.getDish(id);
  }

  goBack(): void {
    this.location.back();
  }
}

